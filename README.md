# 42-Darkly

Il s'agit du projet Darkly de l'école 42, il faut trouver 14 failles de sécurité sur une application Web.

# 42-Darkly
## Projet Darkly | Projet Sécurité | Ecole 42
### Résumé:<br>
* Ce projet est une introduction à la sécurité web en informatique. *

## Objectifs
Ce projet a pour but de nous faire découvrir, via une application web, la sécurité dans la partie Web.
<br>
Les méthodes que nous allons utiliser, plus ou moins complexes, nous feront voir différemment les applications web.
<br>
Durant ce projet, nous allons devoir trouver 14 failles se trouvant sur une application Web.
Chaque faille nous donnera un flag prouvant que la faille est exploité.

Il est possible de trouver dans les différents dossiers la manière dont nous nous y sommes pris afin de trouver les failles et de les exploiter.
